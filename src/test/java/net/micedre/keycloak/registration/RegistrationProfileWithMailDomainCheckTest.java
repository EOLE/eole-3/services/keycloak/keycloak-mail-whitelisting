package net.micedre.keycloak.registration ;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.junit.Test;
import org.keycloak.authentication.forms.RegistrationPage;
import org.keycloak.events.Errors;
import org.keycloak.models.utils.FormMessage;
import org.keycloak.services.messages.Messages;

public class RegistrationProfileWithMailDomainCheckTest
{
    private static final Pattern ALLOWED_USERNAME_PATTERN = Pattern.compile("^[0-9a-zA-Z\\._]+$");
    
    
    private static final boolean globmatches(String text, String glob, int level) 
    {
        if (text.length() > 200) {
           return false;
        }
        String rest = null;
        int pos = glob.indexOf('*');
        if (pos != -1) {
           rest = glob.substring(pos + 1);
           glob = glob.substring(0, pos);
        }

        if (glob.length() > text.length())
        {
            System.out.println("level="+level+" text="+text+" glob="+glob+" rest="+rest + " : len glob > len text -> false");
            return false;
        }

        // handle the part up to the first *
        for (int i = 0; i < glob.length(); i++)
        {
           char c = glob.charAt(i);
           if (c != '?' )
           {
              String globPart = glob.substring(i, i + 1);
              String textPart = text.substring(i, i + 1);
              
              if( !globPart.equalsIgnoreCase(textPart) )
              {
                  System.out.println("level="+level+" text="+text+" glob="+glob+" rest="+rest + " : glob part != text part -> false");
                  return false;
              }
           }
           else
           {
               System.out.println("level="+level+" text="+text+" glob="+glob+" rest="+rest + " : ignore c="+ c);
           }
        }

        System.out.println("text="+text+" glob="+glob+" rest="+rest +" : match continue");
        
        // recurse for the part after the first *, if any
        if (rest == null)
        {
           if ( glob.length() == text.length() )
           {
               System.out.println("level="+level+" text="+text+" glob="+glob+" rest="+rest + " : rest null, len glob = len text -> true");
               return true;
           }
           else
           {
               System.out.println("level="+level+" text="+text+" glob="+glob+" rest="+rest + " : rest null, len glob != len text -> false");
               return false;
           }
        }
        else
        {
           for (int i = glob.length(); i <= text.length(); i++)
           {
              String sub = text.substring(i);
              if (globmatches(sub, rest, level+1))
              {
                  System.out.println("level="+level+" text="+text+" glob="+glob+" MATCHES !");
                  return true;
              }
           }
           System.out.println("level="+level+" text="+text+" glob="+glob+" -> false");
           return false;
        }
     }

     public boolean validate(String username, String email, String validDomains)
     {
        boolean emailDomainValid = false;
        String eventError = Errors.INVALID_REGISTRATION;

        if( username.indexOf("@") > 0 || ( ALLOWED_USERNAME_PATTERN.matcher(username).matches() == false ) )
        {
            System.err.println(eventError);
            return false;
        }
        if(email == null){
           System.err.println(eventError);
           return false;
        }

        String[] domains = validDomains.split("##");
        for (String domain : domains) {
           if (email.endsWith("@" + domain)) {
              System.out.println("ok email="+email+" pour "+domain);
              emailDomainValid = true;
              break;
           } else if (globmatches(email, "*@" + domain, 0)) {
              emailDomainValid = true;
              break;
           }
        }
        return emailDomainValid;
     }



    @Test
    public void test01()
    {
        StringBuffer whiteDomains = new StringBuffer(1000);
        whiteDomains.append("ac-dijon.fr##");
        whiteDomains.append("education.gouv.fr##");
        whiteDomains.append("ac-toulouse.fr##");
        whiteDomains.append("ac-guyane.fr##");
        whiteDomains.append("ac-versailles.fr##");
        whiteDomains.append("ac-reims.fr##");
        whiteDomains.append("ac-nancy-metz.fr##");
        whiteDomains.append("ac-paris.fr##");
        whiteDomains.append("ac-creteil.fr##");
        whiteDomains.append("ac-strasbourg.fr##");
        whiteDomains.append("ac-lyon.fr##");
        whiteDomains.append("igesr.gouv.fr##");
        whiteDomains.append("hceres.gouv.fr##");
        whiteDomains.append("recherche.gouv.fr##");
        whiteDomains.append("enseignementsup.gouv.fr##");
        whiteDomains.append("ac-grenoble.fr##");
        whiteDomains.append("reseau-canope.fr##");
        whiteDomains.append("ac-cned.fr##");
        whiteDomains.append("clemi.fr##");
        whiteDomains.append("ac-nice.fr##");
        whiteDomains.append("ac-besancon.fr##");
        whiteDomains.append("ac-rennes.fr##");
        whiteDomains.append("ac-nantes.fr##");
        whiteDomains.append("ac-orleans-tours.fr##");
        whiteDomains.append("ac-montpellier.fr##");
        whiteDomains.append("ac-toulouse.fr##");
        whiteDomains.append("ac-bordeaux.fr##");
        whiteDomains.append("ac-poitiers.fr##");
        whiteDomains.append("ac-limoges.fr##");
        whiteDomains.append("ac-spm.fr##");
        whiteDomains.append("ac-lille.fr##");
        whiteDomains.append("ac-amiens.fr##");
        whiteDomains.append("ac-normandie.fr##");
        whiteDomains.append("ac-rouen.fr##");
        whiteDomains.append("ac-caen.fr##");
        whiteDomains.append("ac-noumea.nc##");
        whiteDomains.append("loyalty.nc##");
        whiteDomains.append("province-iles.nc##");
        whiteDomains.append("province-nord.nc##");
        whiteDomains.append("province-sud.nc##");
        whiteDomains.append("ac-polynesie.pf##");
        whiteDomains.append("ac-wf.wf##");
        whiteDomains.append("ac-corse.fr##");
        whiteDomains.append("ac-guadeloupe.fr##");
        whiteDomains.append("ac-reunion.fr##");
        whiteDomains.append("ac-martinique.fr##");
        whiteDomains.append("ac-mayotte.fr##");
        whiteDomains.append("ac-aix-marseille.fr##");
        whiteDomains.append("region-academique-aura.fr##");
        whiteDomains.append("region-academique-auvergne-rhone-alpes.fr##");
        whiteDomains.append("region-academique-bfc.fr##");
        whiteDomains.append("region-academique-bourgogne-franche-comte.fr##");
        whiteDomains.append("region-academique-bretagne.fr##");
        whiteDomains.append("region-academique-centre-val-de-loire.fr##");
        whiteDomains.append("region-academique-corse.fr##");
        whiteDomains.append("region-academique-grand-est.fr##");
        whiteDomains.append("region-academique-guadeloupe.fr##");
        whiteDomains.append("region-academique-guyane.fr##");
        whiteDomains.append("region-academique-hauts-de-france.fr##");
        whiteDomains.append("region-academique-idf.fr##");
        whiteDomains.append("region-academique-ile-de-france.fr##");
        whiteDomains.append("region-academique-martinique.fr##");
        whiteDomains.append("region-academique-normandie.fr##");
        whiteDomains.append("region-academique-mayotte.fr##");
        whiteDomains.append("region-academique-nouvelle-aquitaine.fr##");
        whiteDomains.append("region-academique-occitanie.fr##");
        whiteDomains.append("region-academique-paca.fr##");
        whiteDomains.append("region-academique-provence-alpes-cote-dazur.fr##");
        whiteDomains.append("region-academique-pays-de-la-loire.fr##");
        whiteDomains.append("region-academique-reunion.fr##");
        whiteDomains.append("renater.fr##");
        whiteDomains.append("hceres.fr##");
        whiteDomains.append("reseau-canope.fr##");
        whiteDomains.append("cned.fr##");
        whiteDomains.append("ac-cned.fr##");
        whiteDomains.append("sports.gouv.fr##");
        whiteDomains.append("jeunesse-sports.gouv.fr##");
        whiteDomains.append("service-civique.gouv.fr##");
        whiteDomains.append("diges.gouv.fr##");
        whiteDomains.append("onisep.fr##");
        whiteDomains.append("ac-clermont.fr##");
        whiteDomains.append("pix.fr##");
        whiteDomains.append("mlfmonde.org##");
        whiteDomains.append("*aefe.fr##");
        whiteDomains.append("educagri.fr##");
        whiteDomains.append(".fr##");
        whiteDomains.append("region-academique-*.fr##");
        
        assertTrue( validate("test", "test@aefe.fr", whiteDomains.toString() ));
        assertFalse( validate("test1", "test.aefe.fr@gmail.com", whiteDomains.toString() ));
        assertFalse( validate("toto", "test@toto.fr", whiteDomains.toString() ));
        assertTrue( validate("ra", "ratest@region-academique-test.fr", whiteDomains.toString() ));
    }

    @Test
    public void test02()
    {
        StringBuffer whiteDomains = new StringBuffer(1000);
        whiteDomains.append("region-??ademique-*.fr##");
        
        assertTrue( validate("ra", "ratest@region-academique-test.fr", whiteDomains.toString() ));
    }
    
    @Test
    public void test03()
    {
        StringBuffer whiteDomains = new StringBuffer(1000);
        whiteDomains.append("region-??ademique-*.fr##");
        
        assertTrue( validate("ra", "ratest@region-academique-test.fr", whiteDomains.toString() ));
        assertFalse( validate(" ra", "ratest@region-academique-test.fr", whiteDomains.toString() ));
    }

    @Test
    public void test04()
    {
        StringBuffer whiteDomains = new StringBuffer(1000);
        whiteDomains.append("region-??ademique-*.fr##");
        
        assertTrue( validate("parenthese", "ratest@region-academique-test.fr", whiteDomains.toString() ));
        assertFalse( validate(" (parenthese", "ratest@region-academique-test.fr", whiteDomains.toString() ));
    }
    
}
