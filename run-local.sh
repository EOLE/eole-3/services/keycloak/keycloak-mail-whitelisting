#!/bin/bash 
set -e

echo "REDEPLOIE=$1"
# shellcheck disable=SC1091
source ./function.sh

VERSION="$(getVersionArtifact)"
echo "$VERSION"

#loginHarbor

KEYCLOAK_CONTAINER="check-mail"
VERSION=latest
IMAGE="${KEYCLOAK_CONTAINER}:${VERSION}"

HASH_TAG=$(docker build -q .)
echo "HASH_TAG=$HASH_TAG"
REDEPLOIE="${1:-non}"
echo "REDEPLOIE=$REDEPLOIE"
if [ "$HASH_TAG" != "$(cat /tmp/${KEYCLOAK_CONTAINER}.hash 2>/dev/null)" ]
then
    echo "Hash différent -> redéploie"
    REDEPLOIE=oui
    if ! docker build -t "$IMAGE" .
    then
        exit 1
    fi
    CT_ID=$(docker ps |awk '/'${KEYCLOAK_CONTAINER}'/ {print $1;}')
    echo "CT_ID=$CT_ID"
    if [ -n "$CT_ID" ]
    then
        echo "container démarré -> stop $CT_ID"
        if [ "$REDEPLOIE" == oui ]
        then
            echo "stop $CT_ID"
            docker stop "$CT_ID"
            docker rm "$CT_ID"
        fi
    else
        echo "container non démarré -> déploie"
    fi
else
    echo "Hash indentique -> pas de redéploie"
fi
if [ "$REDEPLOIE" == oui ]
then
    echo "différent -> redéploie"
    dockerCleanAll
    docker run --name "${KEYCLOAK_CONTAINER}" \
           -p 8080:8080 \
           -e DB_VENDOR=H2 \
           -e KEYCLOAK_USER=admin \
           -e KEYCLOAK_PASSWORD=admin \
           -e KEYCLOAK_WELCOME_THEME=keycloak \
           -e KEYCLOAK_DEFAULT_THEME=apps-theme \
           "$IMAGE" &
           #> "${KEYCLOAK_CONTAINER}.log"  &

           #-e KEYCLOAK_IMPORT=/tmp/realm-export.json \
    echo "$HASH_TAG" >/tmp/"${KEYCLOAK_CONTAINER}".hash
    sleep 20
else
    echo "identique pas de déploie"
fi
waitConteneur "${KEYCLOAK_CONTAINER}"

# attente : Keycloak 15.0.2 (WildFly Core 15.0.1.Final) started in 20195ms - Started 1079 of 1373 services (713 services are lazy, passive or on-demand)
waitUrl http://localhost:8080/ 10
waitUrl http://localhost:8080/auth/realms/master/.well-known/openid-configuration 1

initToolsKcAdm

#########################################
# login realm
#########################################
if ! loginKeycloack
then
   exit 1
fi
# actualise le cache users !
getUsers
getUserId "admin"

#KEYCLOAK_REALM=test
#createRealm
#waitUrl http://localhost:8080/auth/realms/test/.well-known/openid-configuration 1

#########################################
# groups
#########################################
getGroups
gidAdmin=$(getGroupId admin)

#########################################
# users
#########################################
getUsers
createUser "gilles" "eole" "$gidAdmin"
createUser "test" "test" "$gidAdmin"
createUser " test" "test" "$gidAdmin"

# actualise le cache users !
getUserId "gilles"
getUserId "test"
getUserId " test"

getClients

firefox "http://localhost:8080/auth/admin/master/console/#/realms/master"

